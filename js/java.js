/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
    TextTyped();
    ArrowsKeys();
    showImg();
});


function TextTyped() {

   $('#header .typed-cursor').css({'display': 'none'});
   $("#header span").typed({
        strings: [" Hi! This is my resume."],
        typeSpeed: 200,
        backSpeed: 200

    });

}
var segment = 0;
function ArrowsKeys() {
    $(document).keyup(function(event) {
        var key = event.which;
        if(key == 37) { // Left arrow key
            slide('left');
        }
        if(key == 39) { //Right arrow key
            slide('right');
        }
    });
    $(document).keydown(function(event) {
        var key = event.which;
        if(key == 39) { // Right arrow key
            var img = document.getElementsByClassName("arrowR");
            img[0].src = ["img/aR2.png"];
            img[1].src = ["img/aR2.png"];
        }
        if(key == 37) { //Left arrow key
            var img = document.getElementById("imgClickAndChange");
            img.src = ["img/aL2.png"];
        }
    });

}

function slide(dir) {
    if (dir == 'left') {
        $(".wrapper").not(":animated").animate({ scrollLeft: "-=540px" }, 500);
        //var img = document.getElementsByClassName("leftArrow");
        var img = document.getElementById("imgClickAndChange");
        img.src = ["img/aL.png"];
    }else{
        $(".wrapper").not(":animated").animate({ scrollLeft: "+=540px" }, 500);
        // var img = document.getElementById("imgClickAndChange2");
        var img = document.getElementsByClassName("arrowR");
        img[0].src = ["img/aR.png"];
        img[1].src = ["img/aR.png"];
    }
}
function showImg() {
    var img = document.getElementsByClassName('cross');
    img.style.visibility = 'visible';
}
